from evdev import InputDevice, list_devices, ecodes
from simpleusbrelay import simpleusbrelay

toggleState=False

#Get the correct device by listing then filtering
devices = map(InputDevice, list_devices())
for dev in devices:
    if dev.name=="Motorola Motorola NYXboard, 2.4GHz device" and "input0" in dev.phys:
        iDev=dev

for event in iDev.read_loop():
    if event.type == ecodes.EV_KEY:
        #Detect 'Green' button
        if event.code==62 and event.value==1:
            relaycontroller=simpleusbrelay(Vendor=0x16c0, Product=0x05df)
#             print("Found code 62")
#             print(event.value)
            if toggleState:
#                 print("Turning off relay")
                relaycontroller.array_off('all')
                toggleState=False
            else:
#                 print("Turning on relay")
                relaycontroller.array_on('all')
                toggleState=True


            
            