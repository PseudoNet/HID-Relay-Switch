# HID USB Mains Relay

Rough and ready bit of code hardwired to:

Intercept a green button press from a Motorola NYXboard Hybrid http://www.pulse-eight.com/store/products/96-motorola-nyxboard-hybrid.aspx

When detected it will toggle a usb mains relay http://www.dx.com/p/usb-control-switch-2-channel-5v-relay-module-red-289998

Attached also is a copy of the keyboard.xml file used to override default button functionality of XBMC to allow the relay program to operate in piece.

Included is the simpleusbrelay lib https://github.com/patrickjahns/simpleusbrelay

External Python lib dependancies are:
- evdev
- usb
